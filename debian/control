Source: backup-manager
Section: admin
Priority: optional
Build-Depends-Indep: debiandoc-sgml,
                     ghostscript,
                     texlive-fonts-recommended,
                     texlive-latex-base,
                     texlive-latex-extra,
                     texlive-latex-recommended,
Build-Depends: dar, debhelper (>= 11), gnupg, po-debconf, rsync
Maintainer: Maximiliano Curia <maxy@debian.org>
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/debian/backup-manager
Vcs-Git: https://salsa.debian.org/debian/backup-manager.git
Homepage: https://github.com/sukria/Backup-Manager

Package: backup-manager
Architecture: all
Suggests: anacron,
          backup-manager-doc,
          dar,
          dvd+rw-tools,
          genisoimage,
          gettext-base,
          libnet-amazon-s3-perl,
          openssh-client,
          wodim,
          zip,
Depends: debconf | debconf-2.0,
         ucf (>= 2.009),
         ${misc:Depends},
         ${perl:Depends},
Description: command-line backup tool
 This is a backup program, designed to help you make daily archives of
 your file system.
 .
 Written in bash and perl, it can make tar, tar.gz, tar.bz2, and zip
 archives and can be run in a parallel mode with different
 configuration files. Other archives are possible: MySQL or SVN dumps,
 incremental backups...
 .
 Archives are kept for a given number of days and the upload system
 can use FTP, SSH or RSYNC to transfer the generated archives to a list of
 remote hosts.
 .
 Automatically burning archives to removable media such as CD or DVD is also
 possible.
 .
 The configuration file is very simple and basic and gettext is used for
 internationalization.

Package: backup-manager-doc
Section: doc
Architecture: all
Suggests: backup-manager
Depends: ${misc:Depends}
Description: documentation package for Backup Manager
 Backup-manager is a backup program, designed to help you make daily
 archives of  your file system.
 .
 This package provides the Backup Manager User Guide in different formats:
 HTML, plain text and PDF.
